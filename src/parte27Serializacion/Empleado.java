package parte27Serializacion;

import java.io.Serializable;

/*
 * El objeto a serializar debe implementar la interface 
 * Serializable
 */
class Empleado implements Comparable<Empleado>, Serializable {

	/**
	 * Se agrega el Serial Version ID para saber en que versi�n del programa se
	 * realiz� esta Clase si el Serial Version ID es distinto cuando ser 
	 * grab� del momento en el que se realiz� la lectura el programa
	 * nos tirar� un error
	 */

	static final long serialVersionUID = 1L;

	private int legajo;
	private String nombre;

	public Empleado(int legajo, String nombre) {
		this.legajo = legajo;
		this.nombre = nombre;
	}

	public int getLegajo() {
		return legajo;
	}

	public void setLegajo(int legajo) {
		this.legajo = legajo;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	@Override
	public String toString() {
		return legajo + ") " + nombre;
	}

	@Override
	public int compareTo(Empleado e) {
		// TODO Auto-generated method stub
		return this.legajo - e.legajo;
	}

}
