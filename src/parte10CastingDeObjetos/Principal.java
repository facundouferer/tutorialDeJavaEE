package parte10CastingDeObjetos;

class Principal {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		System.out.println(Empleado.getFacultad()+"\n");

		Empleado[] empleados = new Empleado[3];

		empleados[0] = new Empleado(12332123, "Carlos");
		/*
		 * UPCASTING: cuando almacenamos un clase en su superclase. 
		 * es decir una clase de abajo, la almacenamos arriba. 
		 */
		empleados[1] = new Profesor(25147789, "Sabrina", "ISI"); //UpCasting o Casting impl�cito 
		empleados[2] = new Empleado(12951478, "Rita");

		
		for (Empleado empleado : empleados) {			
			System.out.println(empleado.toString());
		}
		
		System.out.println("-------------");
		
		/*
		 * DOWNCASTING: es cuando almacenamos una superclase en su clase
		 * pero no se puede hacer as� nom�s. 
		 * Primero debemos CASTEAR a la superclase para que entre en la clase hija. 
		 * porque si bien un Empleado es un profesor, no todo empleado 
		 * es profesor. 
		 */
		
		Profesor[] profesores = new Profesor[3];

		profesores[0] = new Profesor(12332123, "Carlos", "TUP");
		
		Empleado empleadoParaCastear =  new Profesor(25147789, "Sabrina", "TUP"); //DOWNCASTING o casting expl�cito 1) hago Upcasting
		profesores[1] = (Profesor) empleadoParaCastear; //DOWNCASTING o casting expl�cito 2) logro el DownCasting
		
		profesores[2] = new Profesor(12951478, "Rita", "IQ");
		
		for (Empleado profesor : profesores) {			
			System.out.println(profesor.toString());
		}

	}

}
