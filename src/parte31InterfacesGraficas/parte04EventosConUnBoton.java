package parte31InterfacesGraficas;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class parte04EventosConUnBoton {

    public static void main(String[] args) {
        // TODO Auto-generated method stub
        VentanasPrincipales ventana = new VentanasPrincipales();
        ventana.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

    }
}

class VentanasPrincipales extends JFrame{

    public VentanasPrincipales() {

        setBounds(5,50,500,400);
        PanelesConBotones panel = new PanelesConBotones();
        add(panel);
        setVisible(true);
    }

}

//implementar ActionListener para escuchar la accion
class PanelesConBotones extends JPanel implements ActionListener {

    JButton botonAzul = new JButton("AZUL");

    public PanelesConBotones () {
        add(botonAzul);
        botonAzul.addActionListener(this);
    }

    //metodo de la interface ActionListener
    @Override
    public void actionPerformed(ActionEvent e) {
        // TODO Auto-generated method stub
        setBackground(Color.BLUE);
    }

}
