package parte31InterfacesGraficas;

import javax.swing.*;

public class parte01PrimerJFrame {
    public static void main(String[]args){

        JFrame miventana = new JFrame();
        /*
        la ventana por defecto es invisible
        y tiene 0 pixeles.
         */
        //setSize da tama�o

        miventana.setSize(500,400);

        //dice que se hara cuando el usuario aprete la X
        //Se pes pasa los campos de clases de WindowsConstatn
        miventana.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        //al apretar la X no cierra, solo oculta la ventana
        //miventana.setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);

        //Indica donde aparecera la ventana
        miventana.setLocation(50,50);

        //hace visible la ventana
        miventana.setVisible(true);

    }
}
