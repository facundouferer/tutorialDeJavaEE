package parte11ClaseAbstracta;

class Principal {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		Profesores[] profesores = new Profesores[3];

		profesores[0] = new Profesores(12332123, "Carlos", "TUP");
		
		Personas empleadoParaCastear =  new Profesores(25147789, "Sabrina", "TUP"); //DOWNCASTING o casting explícito 1) hago Upcasting
		profesores[1] = (Profesores) empleadoParaCastear; //DOWNCASTING o casting explícito 2) logro el DownCasting
		
		profesores[2] = new Profesores(12951478, "Rita", "IQ");
		
		for (Personas profesor : profesores) {			
			System.out.println(profesor.toString());
		}

	}

}
