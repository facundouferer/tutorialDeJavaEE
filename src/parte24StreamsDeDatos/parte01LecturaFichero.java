package parte24StreamsDeDatos;

import java.io.FileReader;

public class parte01LecturaFichero {

   public static void main(String[]args) {

      LeerFichero accesoExternos = new LeerFichero();
      accesoExternos.leeDatos();
   }

}

class LeerFichero{

   public void leeDatos() {
      /*
      entrada es el flujo de datos. El tunel por donde ir�n los datos.
       */
      try {
         FileReader entrada = new FileReader("C:/Users/facundo/eclipse-workspace/tutorialDeJavaEE/src/parte27StreamsDeDatos/archivo.txt");
         /*
         nos devuelve el n�mero correspondeinte al caracter UNICODE o -1 si est� en el final
          */
         int caracter = entrada.read();
         char letra =(char)caracter;

         while(caracter != -1){
            /*
            preguntamos por -1 para saber cuando termina.
            la variable caracter tiene el nro equivalente al codigo UNICODE de cada letra.
             */
            System.out.print(letra);
            caracter = entrada.read();
            letra =(char)caracter;

         }
         /*
         siempre hay que cerrar los bufers o accesos a ficheros externos.
          */
         entrada.close();

      } catch (Exception e) {
         e.printStackTrace();
      }

   }

}
