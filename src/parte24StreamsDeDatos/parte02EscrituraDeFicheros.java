package parte24StreamsDeDatos;

import java.io.FileWriter;

public class parte02EscrituraDeFicheros {

    public static void main(String[]args){

        Escritura escribeElArchivo = new Escritura();
        escribeElArchivo.escribir();

    }

}

class Escritura{

    public void escribir(){

        String texto = "Esto se escribi� en el archivo desde el programa";

        try {
            /* Si en FileWrite pasamos solo el parametro del archivo re escribe todo el archivo
            *  pero si lo que le pasamos son dos parametros y el segundo es true entonces
            * agrega a lo que ya tiene el archivo. */
            FileWriter escriturua = new FileWriter("C:/Users/facundo/eclipse-workspace/tutorialDeJavaEE/src/parte27StreamsDeDatos/archivo.txt", true);
            /*
            Para escribir recorremos caractear a caracter el String y lo escribimos en el archivo.
             */
            for(int i =0; i < texto.length(); i++){
                /* Escribe letra por letra en el archivo.*/
                escriturua.write(texto.charAt(i));
            }

            escriturua.close();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
