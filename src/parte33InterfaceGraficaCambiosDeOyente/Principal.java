package parte33InterfaceGraficaCambiosDeOyente;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;


/** INICIO CLASE PRINCIPAL **/

class parte01EventosConVariosBotones {

    public static void main(String[] args) {
        // TODO Auto-generated method stub
        VentanasPrincipales ventana = new VentanasPrincipales();
        ventana.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

    }
}

/** INICIO VENTANA PRINCIPAL **/
class VentanasPrincipales extends JFrame {

    public VentanasPrincipales() {

        setBounds(5, 50, 500, 400);
        PanelesConBotones panel = new PanelesConBotones();
        setTitle("Botonera cambia colores");
        add(panel);
        setVisible(true);
    }
}


/** INICIO PANEL CON BOTONES **/
    class PanelesConBotones extends JPanel {

        JButton botonAzul = new JButton("AZUL");
        JButton botonRojo = new JButton("ROJO");
        JButton botonVerde = new JButton("VERDE");

        public PanelesConBotones() {
            add(botonAzul);
            add(botonRojo);
            add(botonVerde);

            botonAzul.addActionListener(new ColorDeFondo(Color.BLUE));
            botonRojo.addActionListener(new ColorDeFondo(Color.RED));
            botonVerde.addActionListener(new ColorDeFondo(Color.GREEN));

        }

    /** INICIO COLOR DE FONDO **/

        /* Es una clase interna por lo tanto puede acceder a los m�todos de esta clase*/
        private class ColorDeFondo implements ActionListener{

            private Color colorDeFondo;

            ColorDeFondo(Color c){
                this.colorDeFondo = c;
            }

            @Override
            public void actionPerformed(ActionEvent e) {
                setBackground(colorDeFondo);
            }

        }

    }
