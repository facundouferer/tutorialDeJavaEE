package parte13ClaseInternaYanonima;

import java.awt.event.*;
import java.util.Date;

import javax.swing.*;

public class temporizador01 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		/*
		 * 1) Para optimizar el c�digo, directamente en el constructor 
		 * creamos la CLASE INTERNA AN�NIMA
		 */
		Timer myTemporizador = new Timer(500, new ActionListener() {
			/*
			 * 2) Esto es una clase interna an�nima
			 */
			
			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				Date horaActual = new Date();
				System.out.println(horaActual);
				
			}
			
		});

		myTemporizador.start();
		
		JOptionPane.showMessageDialog(null, "metele click");
		

	}

}