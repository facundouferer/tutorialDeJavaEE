package parte13ClaseInternaYanonima;

import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Date;

import javax.swing.JOptionPane;
import javax.swing.Timer;

class temporizador02ConSonido {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		/*
		 * 7) Instanciamos una clase 
		 * de tipo reloj con un par�metro para 
		 * que ejecute sonido cada 3 segundos y true
		 * porque queremos sonido!!!!!
		 */
		Reloj miReloj = new Reloj(3000, true);
		miReloj.ejecutarTemporizador();
		/*
		 * 8) Para seguir ejecutano el programa
		 */
		
		JOptionPane.showMessageDialog(null, "Escuchando sonidos!!!!");
	}

}

/*
 * 1) Creamos la clase Reloj
 */
class Reloj{
	
	/*
	 * 2) Par�metros 
	 */
	private int intervalo; 
	private boolean sonido;	
	
	/*
	 * 3) Constructor 
	 */
	public Reloj(int intervalo, boolean sonido) {
		this.intervalo = intervalo;
		this.sonido = sonido;
	}
	
	/*
	 * 4) Clase interna 
	 */
	public void ejecutarTemporizador() {
		/*
		 * 5) No esoty instanciando una interface
		 * sino una clase, gracias al principio 
		 * de sustituci�n.
		 */
		ActionListener oyente = new DameLaHora();
		
		/*
		 * 6) 
		 */
		Timer miTemporizdor = new Timer(intervalo, oyente);
		miTemporizdor.start();
	}
	
	/*
	 * 9) creamos la clase interna con el modificador 
	 * private que s�lo puede ser colocando en una clase
	 * cuando la clase es interna
	 */
	
	private class DameLaHora implements ActionListener{

		/*
		 * 10) El m�todo que hay que implementar 
		 * porque es una interface. 
		 */
		@Override
		public void actionPerformed(ActionEvent e) {
			// TODO Auto-generated method stub
			/*
			 * 11) Creamos el temporizador con la 
			 * clase Date e imprimimos el d�a y la hora cada
			 * tres segundos. 
			 */
			
			Date ahora = new Date();
			System.out.println(ahora);
			
			/*
			 * 12) Implementamos la ajecuci�n del sonido
			 */
			
			if(sonido) {
				Toolkit.getDefaultToolkit().beep();
			}
			
		}
		
	}
	
}
