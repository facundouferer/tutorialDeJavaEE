package parte13ClaseInternaYanonima;

import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Date;

import javax.swing.JOptionPane;
import javax.swing.Timer;

class temporizador03 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		/*
		 * Sacamos el constructor del programa anterior
		 */
		Reloj03 miReloj = new Reloj03();
		miReloj.ejecutarTemporizador(3000, true);
		
		JOptionPane.showMessageDialog(null, "PULSAR PARA TERMINAR!!!!");
	}

}


class Reloj03{
	

	private int intervalo; 
	private boolean sonido;	
	
	/*
	 * Borrramos el constructor del programa 
	 * anterior
	 */
	

	public void ejecutarTemporizador(int intervalo, boolean sonido) {
	
		/*
		 * Copiamos y pegamos la clase
		 * pero le sacamos el modificador de acceso 
		 * porque no lo puede tener cuando esta dentro de un 
		 * m�todo. 
		 */
		class DameLaHora implements ActionListener{

			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub

				Date ahora = new Date();
				System.out.println(ahora);

				if(sonido) {
					Toolkit.getDefaultToolkit().beep();
				}
				
			}
			
		}
		
		ActionListener oyente = new DameLaHora();

		Timer miTemporizdor = new Timer(intervalo, oyente);
		miTemporizdor.start();
		
	}
	
}
