package parte06POO;

public class ej01POO_Autos {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

}

class Vehiculo {

	private String color;
	private int cantRuedas;
	private double ancho, largo, peso;

	/*
	 * Como se puede ver a continuaci�n hay dos constructores, Esto es lo que
	 * llamar�amos sobrecarga de constructores o sobrecarga De m�todos, que es igual
	 * ya que  un constructor es un m�todo. La sobrecarga de m�todos, es cuando una
	 * clase tienen varios m�todos Que se llaman de la misma forma pero poseen
	 * diferentes par�metros.
	 * 
	 */

	public Vehiculo() {
		super();
		this.color = "sin color";
		this.cantRuedas = 4;
		this.ancho = 1.5;
		this.largo = 2;
		this.peso = 500;
	}

	public Vehiculo(String color, int cantRuedas, double ancho, double largo, double peso) {
		this.color = color;
		this.cantRuedas = cantRuedas;
		this.ancho = ancho;
		this.largo = largo;
		this.peso = peso;
	}

	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}

	@Override
	public String toString() {
		return "Vehiculo [color=" + color + ", cantRuedas=" + cantRuedas + ", ancho=" + ancho + ", largo=" + largo
				+ ", peso=" + peso + "]";
	}

}
