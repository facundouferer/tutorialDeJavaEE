package parte26DesafioGrupalNro2;

import java.util.HashSet;
import java.util.Scanner;
import java.util.Set;

/*
11) El programa debe tener una clase principal llamada Cine.
 */

public class Cine {


    public static void main(String[] args) {

        Set<Espectadores> espectadores = new HashSet<Espectadores>();
        int seguir = 1;

        Scanner entrada = new Scanner(System.in);

        /*
            11.2) Por medio del c�digo crear una sala con capacidad para 3 personas llamada
            �Sala 01� en donde se proyecte la pel�cula �Jocker�.
             */

        System.out.println("CREAR UNA SALA:");

        System.out.print("Nombre: ");
        String nombreSala = entrada.next();

        System.out.print("capacidad: ");
        int capacidadSala = entrada.nextInt();

        Salas sala01 = new Salas(capacidadSala, nombreSala);

        System.out.print("Pel�cula: ");
        String nombrePelicula = entrada.next();

        sala01.setPelicula(nombrePelicula);

        //CARGAR ESPECTADORES

        while (seguir == 1) {
                /*
                11.1) Cargar espectadores pidiendo los nombre, edad, fila y silla al
                usuario por medio de la consola. En caso de que el usuario
                ingrese letras en lugar de n�meros donde no corresponda el
                programa debe finalizar con el error: "ERROR EN EL INGRESO DE DATOS".
                 */

            System.out.println("NUEVO ESPECTADOR");

            System.out.print("Nombre: ");
            String nombre = entrada.next();

            System.out.print("Edad: ");
            int edad = entrada.nextInt();

            System.out.print("Fila: ");
            String fila = entrada.next();

            System.out.print("Silla: ");
            int silla = entrada.nextInt();

            System.out.println("Para cargar otra espectador presione 1, de lo contrario presiones 0.");
            seguir = entrada.nextInt();

            espectadores.add(new Espectadores(nombre, edad, fila, silla));

        }



            /*
            11.3) Asignar a la sala los espectadores cargados
             */

        sala01.setEspectadores(espectadores);

            /*
            11.4) Imprimir la lista de espectadores que se encuentran asignado a la sala
             */
        sala01.getEspectadores();

            /*
            11.5) Crear un acomodador por medio del c�digo
             */
        System.out.println("NUEVO ACOMODADOR: ");

        System.out.print("Nombre: ");
        String nombreAcomodador = entrada.next();

        System.out.print("Edad: ");
        int edadAcomodador = entrada.nextInt();

        Acomodadores acomodador01 = new Acomodadores(nombreAcomodador, edadAcomodador);
            /*
            11.6) Asignar a dicho Acomodador la sala creada anteriormente
             */
        acomodador01.setSala(sala01);

            /*
            11.7) Asignar un sueldo de $50.000 al Acomodador
             */

        System.out.print("Sueldo: ");
        double sueldoAcomodador = entrada.nextDouble();
        acomodador01.setSueldo(sueldoAcomodador);

            /*
            11.8) Mostrar los datos del Acomodador en la consola
             */
        System.out.println(acomodador01.toString());

            /*
            11.9) Crear un Empleado
             */

        Empleados empleado01 = new Empleados("Garcia", 33);

            /*
            11.10) Mostrar los datos del Empleado
             */
        System.out.println(empleado01.toString());


    }
}
