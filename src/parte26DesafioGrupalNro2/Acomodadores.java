package parte26DesafioGrupalNro2;

/*
8) El programa contendrá una clase Acomodadores que heredará de
la clase Empleados e implementará la interfaz ParaAcomodadores.
 */
public class Acomodadores extends Empleados implements ParaAcomodadores {

    private Salas sala;

    public Acomodadores(String nombre, int edad) {
        super(nombre, edad);
        // TODO Auto-generated constructor stub
    }

    public Salas getSala() {

        return this.sala;
    }

    /*
    9) A los Acomodadores se les podrá designar una sala o modificar la sala asignada.
     */
    public void setSala(Salas sala) {
        this.sala = sala;
    }

    public String getTipo() {
        // TODO Auto-generated method stub
        return "Acomodador";
    }

}
