package parte43LayoutsBorderLayout;

import javax.swing.*;
import java.awt.*;

public class LayoutsPrincipal {

    public static void main(String[] args) {
        // TODO Auto-generated method stub

        FramePrincipal framecito = new FramePrincipal();
        framecito.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        framecito.setVisible(true);
    }
}

class FramePrincipal extends JFrame {

    public FramePrincipal() {
        setTitle("Border Layouts");
        setBounds(600, 350, 600, 300);

        PanelPrincial panelcito = new PanelPrincial();
        add(panelcito, BorderLayout.NORTH);

        PanelSecundario panelcito2 = new PanelSecundario();
        add(panelcito2, BorderLayout.SOUTH);

    }

}

class PanelPrincial extends JPanel {

    public PanelPrincial() {

        /**Tipos de Disposiciones de Layouts**/
        setLayout(new BorderLayout());
        add(new JButton("norte"), BorderLayout.NORTH);
        add(new JButton("centro"), BorderLayout.CENTER);
        add(new JButton("este"), BorderLayout.EAST);

    }
}

class PanelSecundario extends JPanel{

    public PanelSecundario(){
        setLayout(new FlowLayout(FlowLayout.LEFT));
        add(new JButton("uno"));
        add(new JButton("dos"));
    }
}