package parte49SwingEventosEnJTextField;

import javax.swing.*;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.text.Document;

class Principal {

    public static void main(String[] args) {

        FramePrincipal framePrincipal = new FramePrincipal();
        framePrincipal.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        framePrincipal.setVisible(true);

    }

}


class FramePrincipal extends JFrame {

    public FramePrincipal() {
        setBounds(50, 50, 400, 400);
        add(new PanelPrincipal());
        setTitle("JTextField");
        setVisible(true);

    }

}

class PanelPrincipal extends JPanel {

    private JTextField texto;

    private JLabel textoLabel;

    public PanelPrincipal() {

        textoLabel = new JLabel();

        texto = new JTextField(20);

        Document documentoTexto = texto.getDocument();

        documentoTexto.addDocumentListener(new ObtenerTexto());

        add(texto);
        add(textoLabel);

    }

    private class ObtenerTexto implements DocumentListener {

        @Override
        public void insertUpdate(DocumentEvent e) {
            textoLabel.setText(texto.getText());
        }

        @Override
        public void removeUpdate(DocumentEvent e) {
            textoLabel.setText(texto.getText());
        }

        @Override
        public void changedUpdate(DocumentEvent e) {

        }
    }

}
