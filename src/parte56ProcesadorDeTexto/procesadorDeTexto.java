package parte56ProcesadorDeTexto;

import javax.swing.*;
import javax.swing.text.StyledEditorKit;
import java.awt.*;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;

class procesadorDeTexto {

    public static void main(String[] args) {
        // TODO Auto-generated method stub

        FramePrincipal mimarco=new FramePrincipal();

        mimarco.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

    }

}

class FramePrincipal extends JFrame {


    public FramePrincipal(){

        setBounds(500,300,550,420);

        PanelPrincipal milamina=new PanelPrincipal();

        setTitle("Procesador de Texto");

        add(milamina);

        setVisible(true);

    }

}

class PanelPrincipal extends JPanel{

    JTextPane areaDeTexto;

    JMenu fuente;
    JMenu estilo;
    JMenu tamanio;


    public PanelPrincipal(){

        setLayout(new BorderLayout());

        JPanel laminamenu=new JPanel();

        JMenuBar mibarra=new JMenuBar();

        //---------------------------------------------------------

        fuente=new JMenu("Fuente");

        estilo=new JMenu("Estilo");

        tamanio =new JMenu("Tamanio");

        //---------------------------------------------------------------


        configura_menu("Arial","fuente","Arial",9,10);

        configura_menu("Courier","fuente","Courier",9,10);

        configura_menu("Verdana","fuente","Verdana",9,10);

        //--------------------------------------------------------

        configura_menu("Negrita","estilo","",Font.BOLD,1);

        configura_menu("Cursiva","estilo","",Font.ITALIC,1);


        ButtonGroup tamagno_letra=new ButtonGroup();

        JRadioButtonMenuItem doce=new JRadioButtonMenuItem("12");

        JRadioButtonMenuItem dieciseis=new JRadioButtonMenuItem("16");

        JRadioButtonMenuItem veinte=new JRadioButtonMenuItem("20");

        JRadioButtonMenuItem veinticuatro=new JRadioButtonMenuItem("24");



        tamagno_letra.add(doce);

        tamagno_letra.add(dieciseis);

        tamagno_letra.add(veinte);

        tamagno_letra.add(veinticuatro);

        doce.addActionListener(new StyledEditorKit.FontSizeAction("cambia_tamano", 12));

        dieciseis.addActionListener(new StyledEditorKit.FontSizeAction("cambia_tamano", 16));

        veinte.addActionListener(new StyledEditorKit.FontSizeAction("cambia_tamano", 20));

        veinticuatro.addActionListener(new StyledEditorKit.FontSizeAction("cambia_tamano", 24));

        tamanio.add(doce);

        tamanio.add(dieciseis);

        tamanio.add(veinte);

        tamanio.add(veinticuatro);


        //-----------------------------------------------------------

        mibarra.add(fuente);

        mibarra.add(estilo);

        mibarra.add(tamanio);

        //------------------------------------------------------------

        laminamenu.add(mibarra);

        add(laminamenu,BorderLayout.NORTH);

        areaDeTexto =new JTextPane();

        add(areaDeTexto,BorderLayout.CENTER);

        JPopupMenu menuEmergente=new JPopupMenu();

        JMenuItem negritaE=new JMenuItem("Negrita");

        JMenuItem cursivaE=new JMenuItem("Cursiva");

        negritaE.addActionListener(new StyledEditorKit.BoldAction());

        cursivaE.addActionListener(new StyledEditorKit.ItalicAction());

        menuEmergente.add(negritaE);

        menuEmergente.add(cursivaE);

        areaDeTexto.setComponentPopupMenu(menuEmergente);

    }

    public void configura_menu(String textoMenu, String menu, String tipo_letra, int estilos, int tamanioLetra){

        JMenuItem elem_menu=new JMenuItem(textoMenu);


        if(menu=="fuente"){

            fuente.add(elem_menu);

            if(tipo_letra=="Arial"){

                elem_menu.addActionListener(new StyledEditorKit.FontFamilyAction("cambia_letra","Arial"));
            }else if(tipo_letra=="Courier"){

                elem_menu.addActionListener(new StyledEditorKit.FontFamilyAction("cambia_letra","Courier"));
            }else if(tipo_letra=="Verdana"){

                elem_menu.addActionListener(new StyledEditorKit.FontFamilyAction("cambia_letra","Verdana"));
            }


        }else if(menu=="estilo"){

            estilo.add(elem_menu);

            if(estilos==Font.BOLD){

                elem_menu.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_N, InputEvent.CTRL_DOWN_MASK));

                elem_menu.addActionListener(new StyledEditorKit.BoldAction());

            }else if(estilos==Font.ITALIC){

                elem_menu.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_K,InputEvent.CTRL_DOWN_MASK));

                elem_menu.addActionListener(new StyledEditorKit.ItalicAction());

            }

        }else if(menu=="tamanio"){

            tamanio.add(elem_menu);

            elem_menu.addActionListener(new StyledEditorKit.FontSizeAction("cambia_tamanio", tamanioLetra));
        }


    }

}