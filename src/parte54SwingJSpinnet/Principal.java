package parte54SwingJSpinnet;

import javax.swing.*;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import java.awt.*;
import java.lang.reflect.Array;

class Principal {

    public static void main (String[]args){

        FramePrincipal framePrincipal = new FramePrincipal();
        framePrincipal.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        framePrincipal.setVisible(true);

    }

}


class FramePrincipal extends JFrame{

    public FramePrincipal(){
        setBounds(50,50, 400, 400);
        add(new PanelPrincipal());
        setTitle("JSPinner");
        setVisible(true);

    }

}

class PanelPrincipal extends JPanel{

    JSpinner spinnetNro, spinnerTexto;
    JLabel texto;

    public PanelPrincipal(){

        /**JSpiner con valores num�ricos por defecto
         * Tambi�n podemos utilizar un cosntructor con SpinnerNumberModel
         * para establecer valores por defecto**/

        spinnetNro = new JSpinner();
        spinnetNro.setPreferredSize(new Dimension(80,30));
        spinnetNro.addChangeListener(new ChangeListener() {
            @Override
            public void stateChanged(ChangeEvent e) {
                texto.setFont(new Font("Arial", Font.PLAIN, (Integer) spinnetNro.getValue()));
                System.out.println(spinnetNro.getValue());
            }
        });

        add(spinnetNro);

        /**JSpiner con un array de textos**/
        String dias[] = {"lunes", "martes", "mi�rcoles", "jueves", "viernes"};
        spinnerTexto = new JSpinner(new SpinnerListModel(dias));

        spinnerTexto.setPreferredSize(new Dimension(80,30));
        spinnerTexto.addChangeListener(new ChangeListener() {
            @Override
            public void stateChanged(ChangeEvent e) {
                texto.setText((String) spinnerTexto.getValue());
            }
        });

        add(spinnerTexto);

        texto = new JLabel("DIAS");
        add(texto);

    }
}
