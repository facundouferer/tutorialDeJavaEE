package parte41MultiplesOyentesDeEventos;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class multiplesOyentesPrincipal01 {

    public static void main(String[] args) {
        // TODO Auto-generated method stub

        Marco_Principal mimarco = new Marco_Principal();

        mimarco.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        mimarco.setVisible(true);

    }
}

class Marco_Principal extends JFrame {

    public Marco_Principal() {

        setTitle("M�ltiples oyentes");

        setBounds(1300, 100, 300, 200);

        Lamina_Principal lamina = new Lamina_Principal();

        add(lamina);
    }

}

class Lamina_Principal extends JPanel {

    JButton boton_cerrar; /**Ser� pasado como par�metro a las ventanas**/

    public Lamina_Principal() {

        JButton boton_nuevo = new JButton("Nuevo");

        add(boton_nuevo);

        boton_cerrar = new JButton("Cerrar todo");

        add(boton_cerrar);

        /**Ponemos el bot�n a la escucha para que cree
         * las ventanas al ser apretado**/

        boton_nuevo.addActionListener(new CreaMarco());

    }

    /**Clase que crear� el las ventanas nuevas**/

    private class CreaMarco implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent e) {
            /**Todas las ventanas tendr�n como par�metro el btn cerrar**/
            MarcoNuevo marquito = new MarcoNuevo(boton_cerrar);
            marquito.setVisible(true);
        }
    }

}

class MarcoNuevo extends JFrame{

    /**Con una variabe est�tica podemos podemos tener un elemento que sea com�n
     * a todas las instancias del objeto**/

    private static int contador=0;

    public MarcoNuevo(JButton boton){
        contador++;
        setTitle("Venana Nro "+contador);
        setBounds(50*contador,50*contador,300,200);

        /**Colocamos el boton a la escucha**/
        boton.addActionListener(new CerrarTodo());
    }

    /**Creamos la clase de nuestras ventanas que
     * cerrar� todas las instancias al mismo tiemop**/

    private class CerrarTodo implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent e) {
            dispose(); /**Cierra todas las ventanas**/
        }
    }

}