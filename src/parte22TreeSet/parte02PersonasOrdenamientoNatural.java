package parte22TreeSet;

import java.util.TreeSet;

class parte02PersonasOrdenamientoNatural {
	
	/*
	 * Lo llamamos de ordenamiento natural porque
	 * o es de mayor a menor o de menor a mayor
	 * o en orden alfabetico.
	 */
	
	public static void main(String[] args) {
	
		TreeSet<Persona> personas = new TreeSet<>();
		
		personas.add(new Persona(12891222, "Carlos Gabriel"));
		personas.add(new Persona(15478632, "Juana Beatriz"));
		personas.add(new Persona(99999898, "Rosa Mar�a"));
		personas.add(new Persona(11251116, "Ricardo Dar�n"));
		
		for (Persona persona : personas) {
			System.out.println(persona.toString());
		}
	}

}


/*
 * Queremos que se ordene debemos implementar
 * la interdace Comparable
 */
class Persona implements Comparable<Persona>{

	private int dni;
	private String nombre;
	
	public Persona(int dni, String nombre) {
		this.dni = dni;
		this.nombre=nombre;
	}
	
	/*
	 * m�todo que nos obliga a implementar
	 * la interface Comparable
	 */
	
	@Override
	public int compareTo(Persona p) {
		// TODO Auto-generated method stub
		/*
		 * Esta resta ayudar� a comparar los elementos:
		 * devolver� 0 si son iguales, un n�mero positivo si 
		 * el dni de la persona actual es mayor o un n�mero
		 * negativo si el dni de la persona ingresada por par�metro es mayor
		 */
		
		return this.dni - p.getDni();
	}

	public int getDni() {
		return dni;
	}

	public void setDni(int dni) {
		this.dni = dni;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	@Override
	public String toString() {
		return dni + ", " + nombre;
	}
	

}