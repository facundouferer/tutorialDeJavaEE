package parte22TreeSet;

import java.util.TreeSet;

class parte01TreeSet {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		TreeSet<String> personas = new TreeSet<>();
		
		personas.add("Carlos");
		personas.add("Anal�a");
		personas.add("Pedro");
		personas.add("Rita");
		
		/*
		 * 1) TreeSet imprimir� en orden alfabetico 
		 * porque implementa una interface llamada Comparable
		 * que tiene un m�todo compareTo(T) que permite 
		 * la comparaci�n por lo tanto la ordenaci�n
		 */
		
		for (String persona : personas) {
			System.out.println(persona.toString());
		}
		
	}

}
