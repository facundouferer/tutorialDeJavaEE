package parte01introduccion;

import java.util.Scanner;

public class ej09desafio08 {

	public static void main(String[] args) {

		/*
		 * DESAFIO 08:
		 * Crea una clase con el nombre de RaizScanner. Al ejecutar el programa nos debe
		 * pedir introducir un n� por consola.
		 * 
		 * Despu�s de introducir el n� y pulsar ENTER, el programa devuelve en consola
		 * la ra�z cuadrada del n�mero.
		 */

		Scanner entrada = new Scanner(System.in);

		System.out.println("ingrese un Nro");

		int nro = entrada.nextInt();

		double raiz = Math.sqrt(nro);

		System.out.print("La ra�z de " + nro + " es " + raiz);

	}

}
