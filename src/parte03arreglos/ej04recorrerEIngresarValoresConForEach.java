package parte03arreglos;

import java.util.Scanner;

import javax.swing.JOptionPane;

public class ej04recorrerEIngresarValoresConForEach {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		// Escanear� la consola en b�squeda de informaci�n ingresada.

		Scanner entrada = new Scanner(System.in);

		String[] nombres = new String[4];

		for (int i = 0; i < nombres.length; i++) {
			System.out.println("introduce tu nombre por favor");
			nombres[i]= entrada.nextLine(); // captura el siguiente string ingresado por consola
		}

		for (String nombre : nombres) {
			System.out.println("-" + nombre);
		}

	}

}
