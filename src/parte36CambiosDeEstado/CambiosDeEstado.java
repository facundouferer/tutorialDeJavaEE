package parte36CambiosDeEstado;

import javax.swing.*;
import java.awt.*;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.event.WindowStateListener;

public class CambiosDeEstado {
    public static void main(String[] args) {

        // TODO Auto-generated method stub
        MarcoEventoVentana miMarco = new MarcoEventoVentana();

        miMarco.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);

        /** Hay que agregar el escuchador **/

        miMarco.addWindowStateListener(new EventosDeVentana());

    }
}

class MarcoEventoVentana extends JFrame {

    public MarcoEventoVentana() {

        setTitle("Cambios de Estado");

        setBounds(30, 30, 600, 500);

        setVisible(true);

    }

}

class EventosDeVentana implements WindowStateListener {

    @Override
    public void windowStateChanged(WindowEvent e) {
        System.out.println("La Ventana cambi� de estado a: "+e.getNewState());
        if(e.getNewState()==Frame.MAXIMIZED_BOTH) System.out.println("MAXIMIZADO ");
        if(e.getNewState()==Frame.ICONIFIED) System.out.println("MINIMIZADO ");
        if(e.getNewState()==Frame.NORMAL) System.out.println("NORMAL ");
    }
}

