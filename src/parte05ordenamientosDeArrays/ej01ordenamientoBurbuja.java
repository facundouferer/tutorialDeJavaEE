package parte05ordenamientosDeArrays;

import java.util.Arrays;

public class ej01ordenamientoBurbuja {

	public static void main(String[] args) {

		/*
		 * ORDENAMIENTO BURBUJA el m�todo de ordenamiento de burbuja (Bubble sort). Este
		 * m�todo revisa y arregla el orden de cada elemento del arreglo compar�ndolo
		 * con el siguiente. El proceso se repite varias veces hasta que se hayan
		 * verificado todos los elementos, ya no sean necesarios m�s cambios y el
		 * arreglo se encuentre ordenado.
		 */

		int arreglo[] = { 6, 4, 2, 5, 3, 1 };
		int cont = 0;
		// m�todo manual de ordenamiento de mayor a menor
		for (int x = 0; x < arreglo.length; x++) {
			for (int i = 0; i < arreglo.length - x - 1; i++) {

				if (arreglo[i] > arreglo[i + 1]) {					
					int tmp = arreglo[i + 1];
					arreglo[i + 1] = arreglo[i];
					arreglo[i] = tmp;
					cont++;
					// esta estructura muestra la matriz para que podamos ver como se va ordenando
					System.out.print(Arrays.toString(arreglo));
					
					System.out.println(" ");
					// fin for para mostrar
				}				
			}
		}
		System.out.println("PASOS: " + cont);
	}

}
