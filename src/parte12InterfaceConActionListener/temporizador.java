package parte12InterfaceConActionListener;

import java.awt.event.*;
import java.util.Date;

import javax.swing.*;

public class temporizador {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		/*
		 * 1) Se creo una variable del timpo Timer y tambi�n 
		 * se import� import java.util.Timer para traerla
		 */
		
		/*
		 * 4) instanciamos temporizador para poder usarlo al crear
		 * la clase Timer
		 */
				
		
		TemporizadorOyente oyente = new TemporizadorOyente();
		
		Timer myTemporizador = new Timer(500, oyente);
		
		/*
		 * 5) para iniciar el temporizador.
		 * 
		 */
		myTemporizador.start();
		
		JOptionPane.showMessageDialog(null, "metele click");
		

	}

}

/*
 * 2) creamos la clase que implementa ActionListener y por lo tanto, como es interface
 * debemos implementar los m�todos que nos pide. por suerte es s�lo uno. 
 */
class TemporizadorOyente implements ActionListener{

	/*
	 * 3) Este m�todo se ejecutar� cada vez
	 * que ocurre una acci�n o evento.
	 */ 

	@Override
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub
		Date horaActual = new Date();
		System.out.println(horaActual);
		
	}
	
}