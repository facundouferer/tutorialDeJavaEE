package parte35ClasesAdaptadoras;

import javax.swing.*;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;

public class parte01ClasesAdaptadoras {
    public static void main(String[] args) {

        // TODO Auto-generated method stub
        MarcoEventoVentana miMarco = new MarcoEventoVentana();

        /*
         * en este caso utilizamos DISPOSE_ON_CLOSE para poder dar
         * tiempo a que aparezcan los eventos de proceso de cierre
         * y de cierre
         */
        miMarco.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);

        /** Hay que agregar el escuchador**/

        miMarco.addWindowListener(new EventosDeVentana());

    }
}

class MarcoEventoVentana extends JFrame {

    public MarcoEventoVentana() {

        setTitle("Clases Adaptadoras");

        setBounds(30, 30, 600, 500);

        setVisible(true);

    }

}

class EventosDeVentana extends WindowAdapter {

    public void windowClosed(WindowEvent e) {
        System.out.println("Adi�s y vuelva pronto");
    }

}
