package parte07UsoEstudiantes;

class Estudiante {
	
	private Carrera carreraEstudiante;
	private String nombre; 
	private double promedio;
	
	private int nroLegajo;
	private static int LegajoAlmacenado=1;
	
	public Estudiante(Carrera carreraEstudiante, String nombre, double promedio) {

		this.carreraEstudiante = carreraEstudiante;
		this.nombre = nombre;
		this.promedio = promedio;
		this.nroLegajo = LegajoAlmacenado;
		Estudiante.LegajoAlmacenado++;
	}
	
	public void setPromedio(double promedio) {
		this.promedio=promedio;
	}

	@Override
	public String toString() {
		return "Estudiante [carreraEstudiante=" + carreraEstudiante + ", nombre=" + nombre + ", promedio=" + promedio
				+ ", nroLegajo=" + nroLegajo + "]";
	}
	
	private String getNombreCarrera() {
		return this.carreraEstudiante.getNombreCarrera();
	}
	
	public String getNombreEstudiante() {
		return this.nombre;
	}

}
