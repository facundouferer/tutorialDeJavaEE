package parte37EventosDeTeclado;


import javax.swing.*;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

public class EventosDeTeclado {

    public static void main(String[] args) {

        MarcoEventoVentana miMarco = new MarcoEventoVentana();

        miMarco.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);

        miMarco.addKeyListener(new EscuchadorDelEventosDeTeclado());

    }

}

class MarcoEventoVentana extends JFrame {

    public MarcoEventoVentana() {

        setTitle("Eventos De Teclado");

        setBounds(30, 30, 600, 500);

        setVisible(true);

    }

}

class EscuchadorDelEventosDeTeclado implements KeyListener{

    @Override
    public void keyTyped(KeyEvent e) {
        System.out.println("pulsando"); /** getKeyCode nos muestra el c�digo de la recla.  */
    }

    @Override
    public void keyPressed(KeyEvent e) {
        System.out.println("presionada: "+e.getKeyChar()); /** getKeyChard nos devuelve el caracter asociado a la tecla.  */

    }

    @Override
    public void keyReleased(KeyEvent e) {
        System.out.println("soltada");
    }
}
