package parte40MultiplesFuentesDeEventos;

import javax.swing.*;
import javax.swing.plaf.ActionMapUIResource;
import java.awt.*;
import java.awt.event.ActionEvent;

public class MultiplesFuenteDeEventosPrincipal {

    public static void main (String[]args){
        FramePrincipal frame = new FramePrincipal();
        frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
    }

}

class FramePrincipal extends JFrame{

    public FramePrincipal(){
        setBounds(50,50,400,200);
        setTitle("M�ltiples Fuentes de Eventos");
        add(new PanelPrincipal());
        setVisible(true);
    }

}

class PanelPrincipal extends JPanel{

    public PanelPrincipal(){

        EventoColorDeFondo colorAzul = new EventoColorDeFondo("Azul", Color.BLUE);
        add(new JButton(colorAzul));

        EventoColorDeFondo colorRojo = new EventoColorDeFondo("Rojo", Color.RED);
        add(new JButton(colorRojo));

        EventoColorDeFondo colorVerde = new EventoColorDeFondo("Verde", Color.GREEN);
        add(new JButton(colorVerde));

        InputMap mapaEntrada = getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW);

        KeyStroke teclaAzul = KeyStroke.getKeyStroke("ctrl A");
        KeyStroke teclaRojo = KeyStroke.getKeyStroke("ctrl R");
        KeyStroke teclaVerde = KeyStroke.getKeyStroke("ctrl V");

        mapaEntrada.put(teclaAzul, "fondo azul");
        mapaEntrada.put(teclaRojo, "fondo rojo");
        mapaEntrada.put(teclaVerde, "fondo verde");

        ActionMap mapaAccion = getActionMap();

        mapaAccion.put("fondo azul", colorAzul);
        mapaAccion.put("fondo rojo", colorRojo);
        mapaAccion.put("fondo verde", colorVerde);
    }


    class EventoColorDeFondo extends AbstractAction{

        public EventoColorDeFondo(String nombre, Color colorFondo){
            putValue(Action.NAME, nombre);
            putValue("color_fondo", colorFondo);
        }
        @Override
        public void actionPerformed(ActionEvent e) {
            Color c = (Color) getValue("color_fondo");
            setBackground(c);
        }
    }

}




