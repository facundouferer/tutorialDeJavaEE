package parte52SwingJComboBox;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

class Principal {

    public static void main (String[]args){

        FramePrincipal framePrincipal = new FramePrincipal();
        framePrincipal.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        framePrincipal.setVisible(true);

    }

}


class FramePrincipal extends JFrame{

    public FramePrincipal(){
        setBounds(50,50, 400, 400);
        add(new PanelPrincipal());
        setTitle("Radio BUtton");
        setVisible(true);

    }

}

class PanelPrincipal extends JPanel{

    JLabel texto;
    JComboBox comboBoxMio;

    public PanelPrincipal() {
        setLayout(new BorderLayout());
        texto = new JLabel("Hola Mundo");
        texto.setFont(new Font("Arial", Font.PLAIN, 18));
        add(texto, BorderLayout.CENTER);

        JPanel panelSecudnario = new JPanel();

        /**
         * Al instanciar el COmboBox podemos agregar los items pormedio de un array
         * o utilizar el constructor por defecto y agregar los items posteriormente
         * uno por uno.
         * **/
        comboBoxMio = new JComboBox(new String[]{"Serif", "Arial Black", "Times New Roman", "Georgia"});

        /**
         * Tambi�n se puede agregar los items al comboBox por separado
         * indicando cada uno de ellos.
         *
        comboBoxMio.addItem("Serif");
        comboBoxMio.addItem("Arial Black");
        comboBoxMio.addItem("Times New Roman");
        comboBoxMio.addItem("Georgia");
         **/

        comboBoxMio.addActionListener(new EscuchadorComboBox());
        panelSecudnario.add(comboBoxMio);
        add(panelSecudnario, BorderLayout.NORTH);

    }

    private class EscuchadorComboBox implements ActionListener{
        @Override
        public void actionPerformed(ActionEvent e) {
            texto.setFont(new Font((String)comboBoxMio.getSelectedItem(), Font.PLAIN, 18));
        }
    }


}
