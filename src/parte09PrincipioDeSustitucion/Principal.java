package parte09PrincipioDeSustitucion;

class Principal {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		Profesor[] profesores = new Profesor[3];

		profesores[0] = new Profesor(12332123, "Carlos", "TSP");
		profesores[1] = new Profesor(25147789, "Sabrina", "ISI");
		profesores[2] = new Profesor(12951478, "Rita", "IQ");

		for (Profesor profesor : profesores) {
			System.out.println(profesor.toString());
		}
		
		
		System.out.println("-------------");

		/*
		 * Por principio de sustituci�n podr�amos almacenar un profesor en un array de
		 * Empleados.
		 * 
		 * si una clase ES UN de otra clase, entonces la primra puede ser almacenada 
		 * en la segunda. 
		 * como un profesor es un epleado, entonces un profesor puede ser almacenado 
		 * en empleados. 
		 */

		Empleado[] empleados = new Empleado[3];

		empleados[0] = new Empleado(12332123, "Carlos");
		empleados[1] = new Profesor(25147789, "Sabrina", "ISI"); //Esto lo podemos hacer por el principio de sustituci�n. 
		empleados[2] = new Empleado(12951478, "Rita");

		
		for (Empleado empleado : empleados) {
			
			/*
			 * a pesar que la variable es de tipo empleado
			 * cuando se ve el toString del Profesor se ocupa el toString() 
			 * de profesores gracias al POLIMORFISMO
			 * dependeindo del contexto una misma variable 
			 * puede comportarse de una forma o de otra. 
			 * Esto tambi�n es posible al ENTRELAZAMIENTO DIN�MICO
			 * que es cuando el interprete JAVA es capaz de detectar
			 * a en cada caso a que m�todo debe llamar. 
			 */
			
			System.out.println(empleado.toString());
		}

	}

}
