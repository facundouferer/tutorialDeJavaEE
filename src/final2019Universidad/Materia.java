package final2019Universidad;

import java.util.Set;

class Materia {
	
	private String nombre; 
	private Set<Estudiante> estudiantes; 
	Profesor profesor;

	public Materia(String nombre, Profesor profesor) {
		super();
		this.nombre = nombre;
		this.profesor = profesor;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public Set<Estudiante> getEstudiantes() {
		return estudiantes;
	}

	public void setEstudiantes(Set<Estudiante> estudiantes) {
		this.estudiantes = estudiantes;
	}

	public Profesor getProfesor() {
		return profesor;
	}

	public void setProfesor(Profesor profesor) {
		this.profesor = profesor;
	}

	@Override
	public String toString() {
		return "Materia [nombre=" + nombre + ", estudiantes=" + estudiantes + ", profesor=" + profesor + "]";
	} 
	
	

}
