package final2019Universidad;

import java.util.Set;

class Facultad {
	
	private Set<Carrera> carreras; 
	private String nombre;
	private String direccion;
	
	public Facultad(Set<Carrera> carreras, String nombre, String direccion) {
		super();
		this.carreras = carreras;
		this.nombre = nombre;
		this.direccion = direccion;
	}

	public Set<Carrera> getCarreras() {
		return carreras;
	}

	public void setCarreras(Set<Carrera> carreras) {
		this.carreras = carreras;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getDireccion() {
		return direccion;
	}

	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}

	@Override
	public String toString() {
		return "Facultad [carreras=" + carreras + ", nombre=" + nombre + ", direccion=" + direccion + "]";
	} 
	
	

}
