package final2019Universidad;

abstract class Persona {
	
	String nombre; 
	int dni;
	
	public abstract int verDNI();
	public abstract void modificarNombre(String nombre);

}
