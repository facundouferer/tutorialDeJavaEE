package parte53SwindJSlider;

import javax.swing.*;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

class Principal {

    public static void main (String[]args){

        FramePrincipal framePrincipal = new FramePrincipal();
        framePrincipal.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        framePrincipal.setVisible(true);

    }

}


class FramePrincipal extends JFrame{

    public FramePrincipal(){
        setBounds(50,50, 400, 400);
        add(new PanelPrincipal());
        setTitle("Radio BUtton");
        setVisible(true);

    }

}

class PanelPrincipal extends JPanel{

    JLabel texto;
    JSlider sliderMio;

    public PanelPrincipal() {

        setLayout(new BorderLayout());
        texto = new JLabel("Hola Mundo");
        texto.setFont(new Font("Arial", Font.PLAIN, 18));

        add(texto, BorderLayout.CENTER);

        JPanel panelSecudnario = new JPanel();

        /**Permite aumentar "cosas"**/
        sliderMio = new JSlider(5, 45, 12);

        sliderMio.setMajorTickSpacing(20); /**Cantidad m�xima de pintas**/
        sliderMio.setMinorTickSpacing(5); /**Separacion **/
        sliderMio.setPaintTicks(true);/** que dibuje las l�neas**/
        sliderMio.setPaintLabels(true); /**Para que se vean los puntos**/

        panelSecudnario.add(sliderMio);
        add(panelSecudnario, BorderLayout.NORTH);

        /**Podemos colocar a la escucha con una clase inerna anonima**/
        sliderMio.addChangeListener(new ChangeListener() {
            @Override
            public void stateChanged(ChangeEvent e) {
                texto.setFont(new Font("Arial", Font.PLAIN, sliderMio.getValue()));
            }
        });

    }



}